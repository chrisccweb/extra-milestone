import React, { Component } from 'react'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button'

class AdministratorBlock extends Component {
    constructor(props) {
        super(props);

        let { accounts, administrators, contractInterface, selectedAccount, selectedAdministrator } = props

        this.state = {
            accounts: accounts || [],
            administrators: administrators || [],
            contractInterface: contractInterface,
            selectedAccount: selectedAccount,
            selectedAdministrator: selectedAdministrator,
        }

        this.handleAdminChange = this.handleAdminChange.bind(this)
        this.handleAccountChange = this.handleAccountChange.bind(this)
        this.addAdmin = this.addAdmin.bind(this)
        this.removeAdmin = this.removeAdmin.bind(this)
    }

    handleAdminChange = event => {
        console.log(event.target.value)
        this.setState({ selectedAdministrator: event.target.value })
    }

    handleAccountChange = event => {
        console.log(event.target.value)
        this.setState({ selectedAccount: event.target.value })
    }

    addAdmin = async event => {
        console.log('Adding an Administrator')
        let admin = this.state.selectedAdministrator || this.state.accounts[0]
        console.log(admin, this.state.selectedAccount)
        let status = await this.state.contractInterface.addAdministrator(this.state.selectedAccount, admin)
        if(status) {
          this.setState({
            administrators: [...this.state.administrators, { key: this.state.administrators.length, data: this.state.selectedAccount } ],
            selectedAdministrator: this.state.selectedAccount
          })
          console.log('Administrator list updated')
        }
    }
    
    removeAdmin = async event => {
        console.log('Removing an Administrator')
        let admin = this.state.selectedAdministrator || this.state.accounts[0]
        let status = await this.state.contractInterface.removeAdministrator(this.state.selectedAccount, admin)
        if(status) {
          let adm = this.state.administrators.filter(element => {
            return (element.data !== this.state.selectedAccount)
          })
          this.setState({administrators: adm})
          console.log('Administrator list updated')
        }
    }

    render() { 
        let AdminList = props => {
            let { admins } = props
            let adminItems = admins.map( element => {
              return <MenuItem value={element.data}>{element.data}</MenuItem>
            })
            return (<Select autoWidth={true} value={this.state.selectedAdministrator} onChange={this.handleAdminChange}>
                      {adminItems}
                    </Select>)
        }
      
        let AccountList = props => {
            let { accounts } = props
      
            let accountItems = accounts.map( element => {
                return <MenuItem value={element.data} >{element.data}</MenuItem>
            })
            return (<Select autoWidth={true} value={this.state.selectedAccount} onChange={this.handleAccountChange}> {accountItems} </Select>)
        }    
          
    return ( 
        <div>
            <h2>Adminstrators</h2>
            <div>
                <AdminList admins={this.state.administrators}/>
                <AccountList accounts={this.state.accounts}/>
                <Button variant="contained" onClick={this.addAdmin}>Add</Button>
                <Button variant="contained" onClick={this.removeAdmin} color="primary">Delete</Button>
            </div>
        </div>
     )
    }
}
 
export default AdministratorBlock;