pragma solidity ^0.4.21;

import "./StandardToken.sol";

contract TeachersPet is StandardToken {
  string public name = "PrOOfOfKnowlEdgE"; 
  string public symbol = "POOOOKEE";
  uint public decimals = 18;
  uint public INITIAL_SUPPLY = 10000 * (10 ** decimals);

  address public owner;
  uint256 public INIT_ADMIN_BALANCE = 1000000;

  constructor() public {
    totalSupply_ = INITIAL_SUPPLY;
    owner = msg.sender;
    balances[owner] = totalSupply_;
    admins[owner] = true;
    administrators.push(owner);
    approve(owner, INIT_ADMIN_BALANCE);
  }

  struct Milestone {
    uint256 reward;
    bytes32 tags;
  }

  struct Student {
    bytes32 id;
    bytes32[] milestones;
    uint tokensReceived;
  }

  bytes32[] public milestoneTitles;
  uint256[] public milestoneRewards;
  bytes32[] public milestoneTags;
  address[] public administrators;
  address[] public registeredStudents;

  mapping(bytes32 => Milestone) milestones;
  mapping(address => Student) students;
  mapping(address => bool) admins;

  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  modifier onlyAdmin() {
    require(admins[msg.sender] == true);
    _;
  }

  event AdminAdded(address _currentAdmin, address _newAdmin, uint256 initialBalance, string msg);
  event AdminRemoved(address _currentAdmin, address _oldAdmin, string msg);
  event MilestoneAdded(address _currentAdmin, bytes32 _newMilestoneTitle, uint256 _reward, bytes32 _tags, string msg);
  event MilestoneRemoved(address _currentAdmin, bytes32 _oldMilestoneTitle, string msg);
  event StudentAdded(address _currentAdmin, address _newStudent, string msg);
  event StudentRemoved(address _currentAdmin, address _oldStudent, string msg);
  event MilestoneAwarded(address _to, uint amount, bytes32 milestone, bytes32 _tags);

  function addAdministrator(address _newAdmin) onlyOwner public {
    require(_newAdmin > 0 && admins[_newAdmin] != true);
    admins[_newAdmin] = true;
    administrators.push(_newAdmin);
    approve(_newAdmin, INIT_ADMIN_BALANCE);
    emit AdminAdded(msg.sender, _newAdmin, INIT_ADMIN_BALANCE, 'Admin added');
  }

  function removeAdministrator(address _oldAdmin) onlyAdmin public {
    require(_oldAdmin > 0);
    require(admins[_oldAdmin] == true);
    require(_oldAdmin != owner);
    delete admins[_oldAdmin];

    for (uint index = 0; index < administrators.length; index++) {
      if(_oldAdmin == administrators[index]) {
        delete administrators[index];
        decreaseApproval(_oldAdmin, allowance(owner, _oldAdmin));
        break;
      }
    }
    emit AdminRemoved(msg.sender, _oldAdmin, 'Admin and allowance removed');
  }

  function getAllAdministrators() public view returns(address[]) {
    return administrators;
  }

  function getAdminAllowance(address _admin) onlyAdmin public view returns(uint256) {
    return allowance(owner, _admin);
  }

  function increaseAdminAllowance(address _admin, uint256 _amount) onlyOwner public returns(uint256) {
    increaseApproval(_admin, _amount);
    return allowance(owner, _admin);
  }

  function decreaseAdminAllowance(address _admin, uint256 _amount) onlyAdmin public returns(uint256) {
    decreaseApproval(_admin, _amount);
    return allowance(owner, _admin);
  }

  function createMilestone(bytes32 _title, uint256 _reward, bytes32 _tags) onlyAdmin public {
    require(_title > 0 && _tags > 0 && _reward >= 0);
    require(milestones[_title].tags == 0);
    Milestone memory milestone = Milestone(_reward,_tags);
    milestones[_title] = milestone;
    milestoneTitles.push(_title);
    milestoneRewards.push(_reward);
    milestoneTags.push(_tags);
    emit MilestoneAdded(msg.sender, _title, _reward, _tags, 'Milestone Added');
  }

  function getMilestone(bytes32 _title) public view returns(bytes32, uint, bytes32){
    require(_title > 0);
    require(milestones[_title].tags > 0);
    return (_title, milestones[_title].reward, milestones[_title].tags);
  }

  function getMilestoneTitles() onlyAdmin public view returns(bytes32[]) {
    return milestoneTitles;
  }

  function getMilestoneRewards() onlyAdmin public view returns(uint256[]) {
    return milestoneRewards;
  }

  function getMilestoneTags() onlyAdmin public view returns(bytes32[]) {
    return milestoneTags;
  }

  function deleteMilestone(bytes32 _title) onlyAdmin public {
    require(_title > 0);
    require(milestones[_title].tags > 0);
    delete milestones[_title];
    for (uint index = 0; index < milestoneTitles.length; index++) {
      if(_title == milestoneTitles[index]) {
        delete milestoneTitles[index];
        delete milestoneRewards[index];
        delete milestoneTags[index];
        break;
      }
    }
    emit MilestoneRemoved(msg.sender, _title, 'Milestone Removed');
  }

  function giveMilestone(address _student, bytes32 _title) onlyAdmin public {
    require(_student > 0);
    require(_title > 0);
    require(students[_student].id > 0);
    require(milestones[_title].tags > 0 || milestones[_title].reward > 0);
    require(allowance(owner, msg.sender) > milestones[_title].reward);

    uint history = students[_student].milestones.length;
    bool duplicate = false;

    // Check for duplicate milestone for student
    for (uint index = 0; index < history; index++) {
      if(_title == students[_student].milestones[index] ) {
        duplicate = true;
        index = history;
      }
    }

    require(duplicate == false);
    students[_student].milestones.push(_title);
    students[_student].tokensReceived += milestones[_title].reward;
    transferFrom(owner, _student, milestones[_title].reward);
    emit MilestoneAwarded(_student, milestones[_title].reward, _title, milestones[_title].tags);
  }

  function createStudent(address _address, bytes32 _id) public {
    require(_address > 0, 'Address not provided');
    require(_id > 0, 'Email not provided');
    require(students[_address].id == 0, 'Student already exist with this address');

    bytes32[] memory studentProgress;
    Student memory student = Student(_id,studentProgress,0);
    students[_address] = student;
    registeredStudents.push(_address);
    emit StudentAdded(msg.sender, _address, 'Student Added');
  }

  function getStudentRecord(address _student) onlyAdmin public view returns(bytes32, bytes32[], uint){
    require(_student > 0);
    require(students[_student].id > 0);
    return (students[_student].id, students[_student].milestones, students[_student].tokensReceived);
  }

  function getAllStudents() onlyAdmin public view returns(address[]) {
    return registeredStudents;
  }

  function deleteStudent(address _student) onlyAdmin public {
    require(_student > 0);
    require(students[_student].id > 0);
    delete students[_student];
    emit StudentRemoved(msg.sender, _student, 'Student Removed');
    if(admins[_student] == true) {
      removeAdministrator(_student);
    }
  }
}
