import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell)

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
})

let count = 0
let id = 0
function createData(email, address, milestones, tokensReceived) {
  id += 1
  return { id, email, address, milestones, tokensReceived }
}

let data = []

let importStudentData = students => {
  return students.map(student => {
    return createData(student.address, student.email, student.milestones, student.tokensReceived)
  })
}

function CustomizedTable(props) {
  const { classes, students } = props
  data = importStudentData(students)

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomTableCell>Email</CustomTableCell>
            <CustomTableCell>Address</CustomTableCell>
            <CustomTableCell numeric>Milestones</CustomTableCell>
            <CustomTableCell numeric>Tokens Awarded</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => {
            return (
              <TableRow className={classes.row} key={n.id}>
                <CustomTableCell numeric>{n.address}</CustomTableCell>
                <CustomTableCell component="th" scope="row">{n.email}</CustomTableCell>
                <CustomTableCell numeric>
                  <List component="nav">
                    {
                      n.milestones.map(milestone => {
                        return ( <ListItem key={count++} button><ListItemText primary={milestone} /></ListItem>)
                      })
                    }
                  </List>
                </CustomTableCell>
                <CustomTableCell numeric>{n.tokensReceived}</CustomTableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </Paper>
  )
}

CustomizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(CustomizedTable)