FROM node:carbon
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install -g npm truffle
RUN npm install

COPY . .
