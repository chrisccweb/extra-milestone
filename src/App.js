import React, { Component } from 'react'
import TeachersPetContract from '../build/contracts/TeachersPet.json'
import getWeb3 from './utils/getWeb3'
import ContractInterface from './utils/contractInterface';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import StudentTable from './components/studentTable'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      web3: '',
      instance: '',
      contractInterface: '',
      accounts: '',
      administrators: [],
      selectedAccount: '',
      selectedAdministrator: '',
      milestones: [],
      selectedMilestoneTitle: '',
      selectedMilestone: '',
      milestoneTitle: '',
      milestoneReward: '',
      milestoneTags: '',
      students: [],
      selectedStudent: '',
      studentAddress: '',
      studentEmail: '',
      tabbedChange: 'Administrators',
    }

    this.addAdmin = this.addAdmin.bind(this)
    this.removeAdmin = this.removeAdmin.bind(this)
    this.createMilestone = this.createMilestone.bind(this)
    this.deleteMilestone = this.deleteMilestone.bind(this)
    this.handleAdminChange = this.handleAdminChange.bind(this)
    this.handleAccountChange = this.handleAccountChange.bind(this)
    this.handleMilestoneChange = this.handleMilestoneChange.bind(this)
    this.handleMilestoneTitle = this.handleMilestoneTitle.bind(this)
    this.handleMilestoneReward = this.handleMilestoneReward.bind(this)
    this.handleMilestoneTags = this.handleMilestoneTags.bind(this)
    this.handleStudentChange = this.handleStudentChange.bind(this)
    this.handleStudentAddressChange = this.handleStudentAddressChange.bind(this)
    this.handleStudentEmailChange = this.handleStudentEmailChange.bind(this)
    this.handleTabChange = this.handleTabChange.bind(this)
  }

  handleAdminChange = event => {
    this.setState({ selectedAdministrator: event.target.value })
  }

  handleAccountChange = event => {
    this.setState({ selectedAccount: event.target.value })
  }

  handleMilestoneChange = event => {
    this.setState({ selectedMilestone: event.target.value })
  }

  handleStudentChange = event => {
    this.setState({ selectedStudent: event.target.value })
  }

  handleMilestoneTitle = event => {
    this.setState({ milestoneTitle: event.target.value})
  }

  handleMilestoneReward = event => {
    this.setState({ milestoneReward: event.target.value })
  }

  handleMilestoneTags = event => {
    this.setState({milestoneTags: event.target.value})
  }

  handleStudentAddressChange = event => {
    this.setState({ studentAddress: event.target.value})
  }

  handleStudentEmailChange = event => {
    this.setState({ studentEmail: event.target.value})
  }

  handleTabChange = event => {
    this.setState({ tabbedChange: event.target.text})
  }

  addAdmin = async event => {
    console.log('Adding an Administrator')
    let admin = this.state.selectedAdministrator || this.state.accounts[0]
    let status = await this.state.contractInterface.addAdministrator(this.state.selectedAccount, admin)
    if(status) {
      let accounts = this.state.accounts.filter(element => {
        return element.data !== this.state.selectedAccount
      })

      this.setState({
        administrators: [...this.state.administrators, { key: this.state.administrators.length, data: this.state.selectedAccount } ],
        selectedAdministrator: this.state.selectedAccount,
        accounts: accounts,
        selectedAccount: accounts[0].data
      })
      console.log('Administrator list updated')
    }
  }

  removeAdmin = async event => {
    if(this.state.administrators.length > 1) {
      console.log('Removing an Administrator')
      let admin = this.state.selectedAdministrator
      let status = await this.state.contractInterface.removeAdministrator(this.state.selectedAdministrator, admin)
      if(status) {
        let adm = this.state.administrators.filter(element => {
          return (element.data !== this.state.selectedAdministrator)
        })
        let accounts = [{key: this.state.accounts.length, data: this.state.selectedAdministrator}, ...this.state.accounts]
        this.setState({
          administrators: adm,
          accounts: accounts,
          selectedAccount: accounts[0].data,
          selectedAdministrator: adm[0].data  
        })
        console.log('Administrator list updated')
      }
    }
    else {
      console.log('Unable to delete the last administrator')
    }
  }

  createMilestone = async event => {
    console.log('Adding a Milestone')
    let admin = this.state.selectedAdministrator
    let status = await this.state.contractInterface.createMilestone(admin, this.state.milestoneTitle, this.state.milestoneReward, this.state.milestoneTags)
    if(status) {
      let milestoneObject = {
        title: this.state.milestoneTitle,
        reward: this.state.milestoneReward,
        tags: this.state.milestoneTags
      }
      this.setState({
        milestones: [...this.state.milestones, milestoneObject],
        selectedMilestone: this.state.milestoneTitle,
        selectedMilestoneTitle: ''
      })
      console.log('Milestone list updated')
    }
  }

  deleteMilestone = async event => {
    console.log('Deleting a Milestone')
    let admin = this.state.selectedAdministrator || this.state.accounts[0]
    let status = await this.state.contractInterface.deleteMilestone(admin, this.state.selectedMilestone)
    if(status) {
      let newMilestoneList = this.state.milestones.filter(element => {
        return element.title !== this.state.selectedMilestone
      })
      this.setState({ milestones: newMilestoneList })
      if(newMilestoneList.length > 0) {
        this.setState({ selectedMilestone: newMilestoneList[0].title })
      }
      console.log('Milestone list updated')
    }
  }

  createStudent = async event => {
    console.log('Creating a Student')
    let admin = this.state.selectedAdministrator || this.state.accounts[0]
    let status = await this.state.contractInterface.createStudent(admin, this.state.studentAddress, this.state.studentEmail, this.state.selectedMilestone)
    if(status) {
      this.setState({
        //accounts: [...this.state.accounts, {key: this.state.accounts.length, data: this.state.studentAddress}],
        //selectedAccount: this.state.studentAddress,
        students: [...this.state.students, { address: this.state.studentAddress, email: this.state.studentEmail, milestones: [], tokensReceived: 0 } ],
        selectedStudent: this.state.studentAddress,
        studentAddress: '',
        studentEmail: ''
      })
      console.log('Student roster updated')
    }
  }

  getAllStudents = async event => {
    console.log('Getting Student Record')
    let admin = this.state.selectedAdministrator || this.state.accounts[0]
    let results = await this.state.contractInterface.getAllStudents(admin)
    if(results) {
      this.setState({ students: results })
    }
    else {
      console.log('Students not found')
    }
  }

  deleteStudent = async event => {
    console.log('Deleting a Student')
    let admin = this.state.selectedAdministrator || this.state.accounts[0]
    let results = await this.state.contractInterface.deleteStudent(admin, this.state.selectedStudent)
    if(results) {
      let updatedRoster = this.state.students.filter(element => {
        return element.address !== this.state.selectedStudent
      })
      this.setState({ students: updatedRoster })
      if(updatedRoster.length > 0) {
        this.setState({selectedStudent: updatedRoster[0].address})
      }
      console.log('Student deleted from roster.')
    }
    else {
      console.log('Students not deleted from roster.')
    }
  }

  giveMilestone = async event => {
    console.log('Giving a reward to a student: ' + this.state.selectedStudent + ' ' + this.state.selectedMilestone)
    let results = await this.state.contractInterface.giveMilestone(this.state.selectedAdministrator, this.state.selectedMilestone, this.state.selectedStudent)
    if(results) {
      let students = await this.state.contractInterface.getAllStudents(this.state.selectedAdministrator)
      this.setState({students})
    }
  }

  async componentWillMount() {
    try {
      let results = await getWeb3
      this.setState({web3: results.web3})
      this.instantiateContract()
    } catch (error) {
      console.log("WEB3 ERROR: " + error.message)
    }
  }

  instantiateContract() {
    const contract = require('truffle-contract')
    const teachersPet = contract(TeachersPetContract)
    teachersPet.setProvider(this.state.web3.currentProvider)

    this.state.web3.eth.getAccounts( async(error, accounts) => {
      let inst = await teachersPet.deployed()
      this.setState({instance: inst})
      let _interface = new ContractInterface(this.state.web3)

      let _admins = await _interface.getAllAdministrators(accounts[0]) 
      let milestoneList = await _interface.getAllMilestones(accounts[0]) 
      let students = await _interface.getAllStudents(accounts[0])

      if(students.length > 0) {
        this.setState({
          selectedStudent: students[0].address
        })
      }

      let count = 0
      let admins = _admins.map(element => {
        return {key: count++, data: element}
      })

      count = 0
      let acc = accounts.filter(element => {
        return _admins.indexOf(element) < 0
      }).map(element => {
        return {key: count++, data: element}
      })

      this.setState({
        contractInterface: _interface, 
        accounts: acc, 
        administrators: admins,
        milestones: milestoneList, 
        selectedAccount: (acc[0] && acc[0].data) || [],
        selectedAdministrator: admins[0].data,
        students: students,
      })
    
      if(milestoneList.length > 0) {
        this.setState({
          selectedMilestone: milestoneList[0].title,
        })
      }
    })
  }

  render() {
    let AdminList = props => {
      let { admins } = props
      let adminItems = admins.map( element => {
        return <MenuItem key={element.data} value={element.data}>{element.data}</MenuItem>
      })
      return (<Select autoWidth={true} value={this.state.selectedAdministrator} onChange={this.handleAdminChange}>
                {adminItems}
              </Select>)
    }

    let AccountList = props => {
      let { accounts } = props
      let out = ''

      if(accounts && accounts.length > 0) {
        let accountItems = accounts.map(element => {
          return <MenuItem key={element.data} value={element.data}>{element.data}</MenuItem>
        })
        out = (<Select autoWidth={true} value={this.state.selectedAccount} onChange={this.handleAccountChange}>
                {accountItems}
               </Select>)
      }

      return out
    }    
    
    let MilestoneList = props => {
      let { milestones, deleteButtonEnabled, rewardButtonEnabled } = props
      let out = ''
      if(milestones && milestones.length > 0) {
        let milestoneItems = milestones.map( element => {
          return <MenuItem key={element.title} value={element.title}>{element.title} - {element.tags}: {element.reward} POK</MenuItem>
        })
                    
        let deleteButton = (deleteButtonEnabled) ? <Button variant="contained" onClick={this.deleteMilestone} color="primary" style={{"margin": "10px"}}>Delete</Button> : ''
        let rewardButton = (rewardButtonEnabled) ? <Button variant="contained" color="primary" onClick={this.giveMilestone} style={{"margin": "10px"}}>Give Milestone</Button> : ''
        out = (<div>
                <Select autoWidth={true} value={this.state.selectedMilestone} onChange={this.handleMilestoneChange}>{milestoneItems}</Select>
                {deleteButton} {rewardButton}
               </div>
              )
      }
      return out
    }

    let StudentList = props => {
      let { students, deleteButtonEnabled } = props
      let out = ''
      if(students && students.length > 0) {
        let studentItems = students.map( element => {
          return <MenuItem key={element.address} value={element.address}>{element.email} : {element.tokensReceived} POK</MenuItem>
        })
                   
        let deleteButton = (deleteButtonEnabled) ? <Button variant="contained" onClick={this.deleteStudent} color="primary" style={{"margin": "10px"}}>Delete</Button> : ''
        out = (<div>
                <Select autoWidth={true} value={this.state.selectedStudent} onChange={this.handleStudentChange}>{studentItems}</Select>
                {deleteButton}
               </div>
              )
      }
      return out
    }

    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal" style={{"zIndex": "1"}}>
          <a href="#" onClick={this.handleTabChange} className="pure-menu-heading pure-menu-link">Administrators</a>
          <a href="#" onClick={this.handleTabChange} className="pure-menu-heading pure-menu-link">Milestones</a>
          <a href="#" onClick={this.handleTabChange} className="pure-menu-heading pure-menu-link">Students</a>
          { this.state.students.length > 0 &&
            <a href="#" onClick={this.handleTabChange} className="pure-menu-heading pure-menu-link">Summary</a>
          }
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-1">
              { this.state.tabbedChange === 'Administrators' &&
                <Grid container spacing={24}>
                  <Grid item xs>
                    <div style={{"textAlign": "center"}}>
                      <h1>Active Administrators</h1>
                      <AdminList admins={this.state.administrators}/>
                    </div>
                  </Grid>
                  { this.state.accounts.length > 0 &&
                    <Grid item xs>
                      <div style={{"textAlign": "center"}}>
                        <h1>Users</h1>
                        <AccountList accounts={this.state.accounts}/>
                      </div>
                    </Grid>
                  }
                  <Grid item xs={12}>
                    <div style={{"textAlign": "center"}}>
                      { this.state.accounts.length > 0 &&
                        <Button variant="contained" onClick={this.addAdmin} style={{"margin": "10px"}}>Add</Button>
                      }
                      { this.state.administrators.length > 1 &&
                        <Button variant="contained" onClick={this.removeAdmin} color="primary" style={{"margin": "10px"}}>Delete</Button>
                      }
                    </div>
                  </Grid>
                </Grid>
              }
              { this.state.tabbedChange === 'Milestones' &&
                <Grid container spacing={24}>
                  <Grid item xs>
                    <div style={{"textAlign": "center"}}>
                      <h1>Active Milestones</h1>
                      <TextField id="title" label="Title" value={this.state.milestoneTitle} onChange={this.handleMilestoneTitle} style={{"margin": "15px"}}/>
                      <TextField id="reward" label="Reward" type="number" value={this.state.milestoneReward} onChange={this.handleMilestoneReward} style={{"margin": "15px"}} />
                      <TextField id="tags" label="Tags" value={this.state.milestoneTags} onChange={this.handleMilestoneTags} style={{"margin": "15px"}} />
                      <Button variant="contained" onClick={this.createMilestone} style={{"margin": "10px"}}>Add</Button>
                    </div>
                  </Grid>
                  <Grid item xs={12}>
                    <div style={{"textAlign": "center"}}>
                      <MilestoneList deleteButtonEnabled={true} milestones={this.state.milestones} style={{"margin": "10px"}}/>
                    </div>
                  </Grid>
                </Grid>
              }
              { this.state.tabbedChange === 'Students' && (this.state.milestones.length === 0 || this.state.students.length === 0) &&
                  <Grid container spacing={24}>
                    <Grid item xs={12}>
                      <div style={{"textAlign": "center"}}>
                        <h1>Active Students</h1>
                        <TextField id="address" label="Address" value={this.state.studentAddress} onChange={this.handleStudentAddressChange} style={{"margin": "15px"}}/>
                        <TextField id="email" label="Email" type="email" value={this.state.studentEmail} onChange={this.handleStudentEmailChange} style={{"margin": "15px"}} />
                        <Button variant="contained" onClick={this.createStudent} style={{"margin": "10px"}}>Add</Button>
                        <StudentList deleteButtonEnabled={true} students={this.state.students} style={{"margin": "10px"}}/>
                      </div>
                    </Grid>                
                  </Grid>                
                }
                { this.state.tabbedChange === 'Students' && this.state.milestones.length > 0 && this.state.students.length > 0 &&
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <div style={{"textAlign": "center"}}>
                        <h1>Active Students</h1>
                        <TextField id="address" label="Address" value={this.state.studentAddress} onChange={this.handleStudentAddressChange} style={{"margin": "15px"}}/>
                        <TextField id="email" label="Email" type="email" value={this.state.studentEmail} onChange={this.handleStudentEmailChange} style={{"margin": "15px"}} />
                        <Button variant="contained" onClick={this.createStudent} style={{"margin": "10px"}}>Add</Button>
                        <StudentList deleteButtonEnabled={true} students={this.state.students} style={{"margin": "10px"}}/>
                      </div>
                    </Grid>                
                    <Grid item xs={12} sm={6}>
                      <div style={{"textAlign": "center"}}>
                        <h1>Give a Milestone</h1>
                        <StudentList deleteButtonEnabled={false} students={this.state.students} style={{"margin": "10px"}}/>
                        <MilestoneList deleteButtonEnabled={false} rewardButtonEnabled={true} milestones={this.state.milestones} style={{"margin": "10px"}}/>
                      </div>
                    </Grid>
                  </Grid>
                }
                { this.state.tabbedChange === 'Summary' &&
                  <Grid container spacing={24}>
                    <Grid item xs>
                      <div style={{"textAlign": "center"}}>
                        <StudentTable students={this.state.students}/>
                      </div>
                    </Grid>
                  </Grid>
                }
            </div>
          </div>
        </main>
      </div>
    )
  }
}

export default App