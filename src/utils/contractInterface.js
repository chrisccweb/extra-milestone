import TeachersPetContract from '../../build/contracts/TeachersPet.json'

const contract = require('truffle-contract')
const teachersPet = contract(TeachersPetContract)
let teachersPetInstance = ''
let web3 = ''

class ContractInterface {
    constructor(props) {
        web3 = props
        this.setupDefaults()
    }

    async setupDefaults() {
        teachersPet.setProvider(web3.currentProvider)
        teachersPet.defaults({from: web3.eth.coinbase});
    }

    getAdminAllowance(_admin) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getAdminAllowance(_admin, {from: _admin})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                console.log('GET ALLOWANCE: ' + error.message)
                resolve(false)
            }
        })
    }    
    
    increaseAdminAllowance(_admin, _owner, _amount) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.increaseAdminAllowance(_admin, {from: _owner, gas: 300000})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                console.log('INCREASE ALLOWANCE: ' + error.message)
                resolve(false)
            }
        })
    }
    
    decreaseAdminAllowance(_admin, _owner, _amount) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.decreaseAdminAllowance(_admin, {from: _owner, gas: 300000})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                console.log('DECREASE ALLOWANCE: ' + error.message)
                resolve(false)
            }
        })
    }

    addAdministrator(newAdminAddress, existingAdminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.addAdministrator(newAdminAddress, {from: existingAdminAddress, gas: 300000})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                console.log('ADD ADMINISTRATOR: ' + error.message)
                resolve(false)
            }
        })
    }

    removeAdministrator(oldAdminAddress, existingAdminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.removeAdministrator(oldAdminAddress, {from: existingAdminAddress})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                console.log('REMOVE ADMINISTRATOR: ' + error.message)
                resolve(false)
            }
        })
    }

    getAllAdministrators(existingAdminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getAllAdministrators({from: existingAdminAddress})
                let filteredResult = result.filter(element => {
                    return element !== '0x0000000000000000000000000000000000000000'
                })
                resolve(filteredResult)
            } catch (error) {
                console.log('GET ALL ADMINISTRATORS: ' + error.message)
                resolve(false)
            }
        })
    }

    createMilestone(adminAddress, title, reward, tags) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.createMilestone(title, reward, tags, {from: adminAddress, gas:300000})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                console.log('CREATE MILESTONE: ' + error.message)
                resolve(false)
            }
        })
    }

    getAllMilestones(adminAddress, title) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let _titles  = await teachersPetInstance.getMilestoneTitles({from: adminAddress})
                let _rewards = await teachersPetInstance.getMilestoneRewards({from: adminAddress})
                let _tags    = await teachersPetInstance.getMilestoneTags({from: adminAddress})

                let titles = _titles.map(element => {
                    return this.convert2Ascii(element)
                }).filter(element => {
                    return element !== ''
                })

                let rewards = _rewards.map(element => {
                    return element.toNumber()
                }).filter(element => {
                    return element > 0
                })

                let tags = _tags.map(element => {
                    return this.convert2Ascii(element)
                }).filter(element => {
                    return element !== ''
                })

                let result = []
                for (let index = 0; index < titles.length; index++) {
                    result.push({
                        title: titles[index], 
                        reward: rewards[index], 
                        tags: tags[index]
                    })
                }
                resolve(result)
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 
    }
    
    getMilestone(adminAddress, title) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getMilestone(title, {from: adminAddress})
                resolve(result)
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 
    }

    getMilestoneTitles(adminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getMilestoneTitles({from: adminAddress})
                let titles = result.map(element => {
                    return this.convert2Ascii(element)
                })
                resolve(titles)
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 

    }

    getMilestoneRewards(adminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getMilestoneRewards({from: adminAddress})
                let reward = result.map(element => {
                    return element.toNumber()
                })
                resolve(reward)
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 
    }

    getMilestoneTags(adminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getMilestoneTags({from: adminAddress})
                let tags = result.map(element => {
                    return this.convert2Ascii(element)
                })
                resolve(tags)
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 
    }

    deleteMilestone(adminAddress, title) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.deleteMilestone(title, {from: adminAddress})
                let messages = result.logs.map(message => {
                    return message.args
                })
                console.log(messages)
                resolve(messages)
            } catch (error) {
                if(error.message.indexOf('revert') >= 0) {
                    console.log('DELETE MILESTONE: Milestone not found')
                }
                else {
                    console.log('DELETE MILESTONE: ' + error.message)
                }
                resolve(false)
            }
        })
    }

    giveMilestone(adminAddress, title, student) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.giveMilestone(student, title, {from: adminAddress, gas: 300000})
                resolve(result)
            } catch (error) {
                console.log('GIVE MILESTONE: ' + error.message)
                resolve(false)
            }
        })
    }

    createStudent(adminAddress, studentAddress, email) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.createStudent(studentAddress, email, {from: adminAddress, gas: 300000})
                let messages = result.logs.map(message => {
                    return message.args
                })
                console.log(messages)
                resolve(messages)
            } catch (error) {
                if(error.message.indexOf('revert') >= 0) {
                    console.log('CREATE STUDENT: Student not created')
                }
                else {
                    console.log('CREATE STUDENT: ' + error.message)
                }
                resolve(false)
            }
        })
    }

    getStudentRecord(adminAddress, student) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.getStudentRecord(student, {from: adminAddress})
                resolve(result)
            } catch (error) {
                if(error.message.indexOf('revert') >= 0) {
                    console.log('GET STUDENT RECORD: Student record not found')
                }
                else {
                    console.log('GET STUDENT RECORD: ' + error.message)
                }
                resolve(false)
            }
        })
    }

    getAllStudents(adminAddress) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let _students  = await teachersPetInstance.getAllStudents({from: adminAddress})
                let students = []
                for (let index = 0; index < _students.length; index++) {
                    const element = _students[index];
                    try {
                        let record = await teachersPetInstance.getStudentRecord(element, {from: adminAddress})
                        if(record[0] !== 0 && record[1] !== 0 && record[2] !== 0) {
                            let email = this.convert2Ascii(record[0])
                            let milestones = record[1].map(element => {
                                return this.convert2Ascii(element)
                            })
                            let tokensReceived = record[2].toNumber()
                            students.push({address: element, email: email, milestones: milestones, tokensReceived: tokensReceived})
                        }
                    } catch (error) {
                        if(error.message.indexOf('revert') < 0) {
                            console.log("GET STUDENT RECORD: " + error.message)
                        }
                    }
                }
                
                let filteredStudents = students.filter(element => {
                    return element !== ''
                })

                resolve(filteredStudents)
            } catch (error) {
                console.log("GET ALL STUDENTS: " + error.message)
                resolve(false)
            }
        }) 
    }

    deleteStudent(adminAddress, student) {
        return new Promise(async resolve => {
            teachersPetInstance = await teachersPet.deployed()
            try {
                let result = await teachersPetInstance.deleteStudent(student, {from: adminAddress})
                let messages = result.logs.map(message => {
                    return message.args
                })
                resolve(parseInt(result.receipt.status,16) && messages)
            } catch (error) {
                if(error.message.indexOf('revert') >= 0) {
                    console.log('DELETE STUDENT RECORD: Student record not found')
                }
                else {
                    console.log('DELETE STUDENT RECORD: ' + error.message)
                }
                resolve(false)
            }
        })
    }

    convert2Ascii(hexCode) {
        let hex = hexCode.toString()
        let str = ''
        for (let i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2) {
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
        }
        return str.replace(/\0/g, '')
    }

}
  
export default ContractInterface  