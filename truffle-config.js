module.exports = {
  compilers: {
    solc: {
      version: "0.4.24"
    }
  },
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*"
    },
    production: {
      host: "localhost",
      port: 8545,
      network_id: "*"
    },
    docker: {
      host: "ganache-cli",
      port: 8545,
      network_id: "*"
    },
    ropsten: {
      host: "localhost",
      port: 8545,
      network_id: "3"
    }
  }
}
