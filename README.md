# Going the Extra Milestone

## Introduction

> A Proof of Knowledge token that incentive students to complete course content and go the extra milestone.

## Docker Installation (requires docker and compose)
```sh
# Clone this repo and change into directory
cd extra-milestone

# Build docker image
docker build . -t extra-milestone

# Start ganache-cli instance and extra-milestone web service (this will take a while ... grab something to drink)
docker-compose up

# Open http://localhost:3000 in your browser (tested in Chrome, FF)
```

Enjoy!!!
